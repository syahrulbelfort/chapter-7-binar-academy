module.exports = {
  games: (req, res) => {
    res.render('gameplay', req.user.dataValues);
  },
};
