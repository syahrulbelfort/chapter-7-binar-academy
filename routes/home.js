const express = require('express');
const home = express.Router();
const homeRoute = require('../controllers/home');

//home routes
home.get('/', homeRoute.index);

module.exports = home;
