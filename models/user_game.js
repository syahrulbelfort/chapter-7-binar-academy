'use strict';
const { Model } = require('sequelize');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
  class User_game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
    static #encrypt = (password) => bcrypt.hashSync(password, 10);
    static register = async ({ nama, email, password }) => {
      const passwordHash = this.#encrypt(password);
      return await this.create({
        nama,
        email,
        password: passwordHash,
        role_id: 2,
      });
    };

    static generateTokenV2 = async ({ id, email }) => {
      const payload = {
        id: id,
        username: email,
      };

      return jwt.sign(payload, SECRETTOKEN);
    };
    checkPassword = (password) => bcrypt.compareSync(password, this.password);
    static authenticate = async ({ email, password }) => {
      try {
        const user = await this.findOne({ where: { email } });
        if (!user) return Promise.reject('User not found!');
        const isPasswordValid = user.checkPassword(password);
        if (!isPasswordValid) return Promise.reject('Wrong password!');
        return Promise.resolve(user);
      } catch (err) {
        return Promise.reject(err);
      }
    };
  }
  User_game.init(
    {
      nama: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'User_game',
      underscored: true,
    }
  );
  return User_game;
};
