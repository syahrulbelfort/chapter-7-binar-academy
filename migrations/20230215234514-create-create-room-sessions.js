'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('create-room-sessions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      kode_session: {
        allowNull: true,
        type: Sequelize.INTEGER,
      },
      home_choice: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      away_choice: {
        allowNull: true,

        type: Sequelize.STRING,
      },
      win: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: {
          model: 'user_games',
          key: 'id',
        },
      },
      room_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: {
          model: 'createRooms',
          key: 'id',
        },
      },
      status: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('create-room-sessions');
  },
};
