const express = require('express');
const bio = express.Router();
const { UserGamesBiodata } = require('../models');

bio.get('/', async (req, res) => {
  try {
    const bio = await UserGamesBiodata.findAll();
    res.status(200).json({
      status: 'success',
      data: bio,
    });
  } catch (err) {
    res.status(500).json({
      status: 'error',
      message: 'Error retrieving user games biodata',
      error: err,
    });
  }
});

bio.post('/', async (req, res) => {
  const { jenis_kelamin, tanggal_lahir, umur } = req.body;
  try {
    const bio = await UserGamesBiodata.create({
      jenis_kelamin,
      tanggal_lahir,
      umur,
    });
    res.status(200).json({
      status: 'success',
      data: bio,
      message: 'Biodata berhasil di tambahkan!',
    });
  } catch (err) {
    res.status(500).json({
      status: 'error',
      message: 'Error adding user games biodata',
      error: err,
    });
  }
});

module.exports = bio;
