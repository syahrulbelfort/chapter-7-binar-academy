const express = require('express');
const router = express.Router();
const { UserGamesHistory, User_game, UserGamesBiodata } = require('../models');

router.get('/', async (req, res) => {
  try {
    const history = await UserGamesHistory.findAll({
      include: [
        {
          model: User_game,
        },
        {
          model: UserGamesBiodata,
        },
      ],
    });
    res.status(200).json({
      status: 'success',
      data: history,
      message: 'ini adalah data history game',
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      status: 'error',
      data: null,
      message: 'Terjadi kesalahan saat mengambil data history game',
    });
  }
});

router.post('/', async (req, res) => {
  const { player_id, bio_id, condition } = req.body;
  try {
    const history = await UserGamesHistory.create({
      player_id,
      bio_id,
      condition,
    });
    res.status(200).json({
      status: 'success',
      data: history,
      message: 'Data history permainan berhasil di tambahkan!',
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      status: 'error',
      data: null,
      message: 'Terjadi kesalahan saat menambah data history permainan',
    });
  }
});

module.exports = router;
