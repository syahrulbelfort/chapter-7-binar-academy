const express = require('express');
const app = express();
const cookieParser = require('cookie-parser');
const session = require('express-session');
const flash = require('express-flash');
const { port = 3100 } = process.env;
const SECRET_KEY = 'secret_key';
const jwt = require('jsonwebtoken');


//VIEW ENGINE EJS
app.set('view engine', 'ejs');

//BUILT IN MIDDLEWARE
app.use(express.static(__dirname + '/public/Home'));
app.use(express.static(__dirname + '/public/Gameplay'));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(
  session({
    secret: 'Buat ini jadi rahasia',
    resave: false,
    saveUninitialized: false,
  })
);

app.use(express.json());
const passport = require('./libs/passport');
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

const routes = require('./routes/router');
app.use('/', routes);

//CREATE & READ A BIODATA
const bioRoute = require('./routes/bio');
app.use('/bio', bioRoute);

// //CREATE USER HISTORY RELATION DATABASE PK
const relationRoute = require('./routes/relation');
app.use('/relation', relationRoute);

//LISTEN PORT
app.listen(port, () => {
  console.log(`server is running on ${port}`);
});
