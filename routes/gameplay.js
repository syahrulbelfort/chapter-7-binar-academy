const express = require('express');
const gameplay = require('../controllers/gameplay');
const game = express.Router();
const restrict = require('../middleware/restrict');

game.get('/games', restrict, gameplay.games);

module.exports = game;
