const express = require('express');
const router = express.Router();
const auth = require('./auth');
const home = require('./home');
const game = require('./gameplay');
const api = require('./room');

router.use('/', auth);
router.use('/', home);
router.use('/', game);
router.use('/api', api);

module.exports = router;
